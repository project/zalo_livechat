<?php

function zalo_livechat_settings_form($form, &$form_state) {
  
  $form['zalo_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Zalo Official Account ID'),
    '#default_value' => variable_get('zalo_account_id', ''),
  );
  $form['zalo_welcome_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Welcome message'),
    '#default_value' => variable_get('zalo_welcome_message', 'Welcome'),
  );
  $form['zalo_autopopup'] = array(
    '#type' => 'textfield',
    '#title' => t('Show after (seconds)'),
    '#default_value' => variable_get('zalo_autopopup', 3),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['zalo_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width (px)'),
    '#default_value' => variable_get('zalo_width', 350),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['zalo_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height (px)'),
    '#default_value' => variable_get('zalo_height', 420),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  
  return system_settings_form($form);
}
